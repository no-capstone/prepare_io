import React from 'react';
import { Navigate, Route, Outlet } from 'react-router-dom';


const ProtectedRoute = () => {
  var token = localStorage.getItem('token')
  return (
    token != null ? <Outlet /> : <Navigate to="/login" />
  )
};

export default ProtectedRoute;