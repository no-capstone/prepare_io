import React from 'react'
import logoImage from '../assets/images/logo.png';
import { ListItem, List, Divider, ListItemText } from '@material-ui/core';


const Settings = () => {
    return (
        <div>
            <div id="reg-page">
                <div>
                    <form>
                        <img src={logoImage}></img>
                        <h1>Settings</h1>
                        <List component="nav" aria-label="mailbox folders">
                            <ListItem button component="a" href="/updatepassword">
                                <ListItemText primary="Change Password" />
                            </ListItem>
                            <Divider />
                            <ListItem button divider component="a" href="/interviewSettings">
                                <ListItemText primary="Interview Settings" />
                            </ListItem>
                            <Divider light />
                        </List>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Settings
