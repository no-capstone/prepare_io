import React from "react";
import '../assets/styles/home.css';
import LandpageImage from '../assets/images/landpage-image.jpeg';


class Home extends React.Component {
  render() {
    return (
      <>
        <div id="home-page">
          <div id="land-text">
            <h1 className="bold">You Can Never Be Too PREPARED</h1>
            <p className="sub-text"><span className="bold">PREPARE.IO</span> aims to bring a new approach to conversation preparation. Our software intends to help users get comfortable in various social environments, and assist in overcoming general social anxiety.</p>
            <span id="start-btn"><a href="/bridgePage">Get Started</a></span>
          </div>
          <img width={"45%"} src={LandpageImage} />
        </div>
      </>
    );
  }
}
export default Home;
