import React from 'react';
import logoImage from '../assets/images/logo.png';
import '../assets/styles/register.css';
import { Select, MenuItem, InputLabel,FormControl, Menu } from '@material-ui/core';
import {useState, useEffect, useLayoutEffect} from 'react';
import axios from '../axios.js';



const ChooseCompanion = () => {

    const [companions, setCompanions] = useState([{}])
    const [selectedCompanion, setSelectedCompanion] = useState({})
  
     const fetchCompanions = async () => {
        axios.get(
        '/api/companions/getAllCompanions'
        ).then((res) => {
          const response = res.data.companions

          setCompanions(response)
       
        })
      }

      const chooseCompanion = (e) => {
        setSelectedCompanion(e.target.value)
      
      }

    useEffect(() => {
         
        fetchCompanions()


      }, [selectedCompanion])
 
      console.log("COMPANIONNNNNN")
      console.log(selectedCompanion)

    return (
        <div>
            <div id="reg-page">
                <form>
                    <img src={logoImage}></img>
                    <h1>Choose Companion</h1>
                    <Select
                        labelId="demo-simple-select-autowidth-label"
                        id="demo-simple-select-autowidth"
                        fullWidth
                        label="Companion"
                        defaultValue=""
                        onChange={chooseCompanion}
                        value={selectedCompanion.title}
                    >
                  
                        {companions.map((companion, index) => (
                          <MenuItem value={companion.title} key={index}>{companion.title}</MenuItem>
                          ))}

                    </Select>

                    <br></br>
                    <br></br>
              <input type="submit" value="Set" className="btnSubmit"></input>
              <br />
            </form>
            <br></br>
            <a href="/settings" id="signUp">Back To Settings Menu </a>
            </div>
        </div>
    )
}

export default ChooseCompanion
