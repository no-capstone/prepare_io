import React, { Component, useEffect, useState } from "react";
import { TextField } from '@material-ui/core';
import '../assets/styles/login.css';
import logoImage from '../assets/images/logo.png';
import axios from '../axios.js';

// import { useNavigate } from "react-router-dom";

// Save the token into session storage
function saveToken(token) {
  localStorage.setItem('token', JSON.stringify(token))

}

// Redirect to home (use hook instead? see: Registration.js)
function navigateToHome() {
  return window.location.replace("/home")
}

const Login = () => {

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  // const navigate = useNavigate()
  // const [validCredentials, setCredentialValidity] = useState(false)
  const [validMsg, setValidMsg] = useState("")

  // useEffect(() => {
  //   if (!validCredentials) {
  //     setValidMsg("Check your credentials and try again!")
  //   } else {
  //     setValidMsg("")
  //   }
  // }, [validMsg])

  const handleSubmit = event => {

    event.preventDefault()
    axios.post(
      'api/users/login',
      {
        email: email,
        password: password
      },
    ).then((res) => {
      console.log(res)
      if((res.data.token) && (res.data.status)) {
        // console.log('token exists')
        const token = res.data.token
        saveToken(token)
        // setCredentialValidity(true)

        navigateToHome()
        // navigate('/home')
      } else {

        let validationMsg = res.data.errorMsg ?? "something went wrong"
        setValidMsg(validationMsg)
        
        // setCredentialValidity(false)
        console.log("Incorrect credentials")
        
      }

    }).catch((response) => {
      if (response.errorMsg) {
        // console.log("HERE")
        console.log(response.errorMsg)
      }
    })    
  }

    return (
      <>
        <div id="login-page">
          <div>
            {/* <form method='POST' action='/check/user'> */}
            <form onSubmit={handleSubmit}>

            <img src={logoImage}></img>
            <h1>Welcome Back</h1>
              <TextField id="standard-basic" name="email" label="Email" fullWidth onChange={(e) => setEmail(e.target.value)} />
              <br></br>
              <TextField id="standard-basic" name="password" label="Password" type="password" fullWidth onChange={(e) => setPassword(e.target.value)} />
              <br /><br />
              {<p id="validMsg">{validMsg}</p>}
              <br /><br />
              <input type="submit" value="Login" className="btnSubmit"></input>
              <br />
              <a href="#">Forgot password?</a>
            </form>
            <p>Don't have an account? <a href="/registration" id="signUp">Sign up!</a></p>
          </div>
        </div>
      </>
    )
  }

export default Login