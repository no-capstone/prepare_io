import React from "react";
import '../assets/styles/about.css';
import robImage from '../assets/images/rob.jpeg';
import seanImage from '../assets/images/sean.jpeg';
import alekseiImage from '../assets/images/aleksei.jpg';
import vincentImage from '../assets/images/vincent.jpeg';


class About extends React.Component {
    render() {
        return (
            <>
            <div id="about-page">
                <div id="about">
                    <h1>About Prepare.io</h1>
                    <p><span className="bold">PREPARE.IO</span> aims to bring a new approach to conversation preparations. Our software intends to help aid in social anxiety and make users feel more comfortable. Based on more interpersonal and public events, the proposed software can prepare for events where users will be required to be speaking to another person by allowing them to first practice with AI. In addition to formal speaking, it will also feature the ability to converse with AI that represents social events such as dates. </p>
                </div>
                
                <div class="card">
                        <div class="rob">
                            <img src={robImage}></img>
                            <h1>Robert Chung</h1>
                            <p class="title">Developer</p>
                            <p>Sheridan College</p>
                            <p><a href="https://www.linkedin.com/in/chungr/"><button>LinkedIn</button></a></p>
                        </div>
                        <div class="aleksei">
                            <img src={alekseiImage}></img>
                            <h1>Aleksei Kheba</h1>
                            <p class="title">Developer</p>
                            <p>Sheridan College</p>
                            <p><a href="https://www.linkedin.com/in/kheba/"><button>LinkedIn</button></a></p>
                        </div>
                        <div class="vincent"> 
                            <img src={vincentImage}></img>
                            <h1>Vincent Irobun</h1>
                            <p class="title">Developer</p>
                            <p>Sheridan College</p>
                            <p><a href="https://www.linkedin.com/in/vincentirobun/"><button>LinkedIn</button></a></p>
                        </div>
                        <div class="sean">
                            <img src={seanImage}></img>
                            <h1>Sean Mapanao</h1>
                            <p class="title">Developer</p>
                            <p>Sheridan College</p>
                            <p><a href="https://www.linkedin.com/in/seanmapanao/"><button>LinkedIn</button></a></p>
                        </div>
                    </div>
            </div>
            </>
        );
    }
}
export default About;