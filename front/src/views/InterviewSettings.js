import React from 'react';
import logoImage from '../assets/images/logo.png';
import '../assets/styles/zoneSettings.css';
import { Select, MenuItem} from '@material-ui/core';
import { useState, useEffect} from 'react';
import axios from '../axios.js';
import { useNavigate } from 'react-router-dom'


const InterviewSettings = () => {

  const navigate = useNavigate();

  //For companions
  const [companions, setCompanions] = useState([{}])
  const [selectedCompanion, setSelectedCompanion] = useState({})

  //For zones
  const [openersVal, setOpenersVal] = useState(0)
  const [middleVal, setMiddleVal] = useState(0)
  const [closersVal, setClosersVal] = useState(0)

  //User ID
  const [userId, setUserId] = useState(0)

  //Validation
  const [valid, setValid] = useState(false)

  const fetchCompanions = async () => {
    axios.get(
      '/api/companions/getAllCompanions'
    ).then((res) => {
      const response = res.data.companions
      console.log(response)
      setCompanions(response)

    })
  }

  const fetchCurrentUserId = async () => {
    const ttoken = localStorage.getItem('token');
    console.log(ttoken)
    axios.post(
      '/api/users/getCurrentUser',
      {
        token: ttoken
      }
    ).then((res) => {
      const id = res.data.currentUser.id
      setUserId(id)
      console.log("id: " + userId)
    })
  }

  //set campion through drop down list
  const chooseCompanion = (e) => {
    setSelectedCompanion(e.target.value)

  }


  const handleSubmit = (e) => {
    e.preventDefault()

    const zones = { openersVal, middleVal, closersVal }

    console.log(zones)



    axios.post(
        '/api/settings/updateInterviewSettings',
        {
          userId: userId,
          companionId: selectedCompanion,
          zones: zones
        }).then((res) => {
            console.log("RESP")
            console.log(res)
        })

        navigate('/bridgePage')
  }

  const isValid = () => {
    if(openersVal !== 0 && middleVal !== 0 && closersVal !== 0 && selectedCompanion > 0){
        setValid(true)
    } else{
      setValid(false)
    }
  }


  useEffect(() => {

    fetchCurrentUserId()
    fetchCompanions()
    isValid()


  }, [selectedCompanion, userId, openersVal, middleVal, closersVal])

console.log(valid)
  return (
    <div>
      <div id="reg-page" style={{ paddingTop: "30px" }}>
        <form onSubmit={handleSubmit}>
          <img src={logoImage}></img>
          <h1>Choose Companion</h1>
          <Select
            labelId="demo-simple-select-autowidth-label"
            id="demo-simple-select-autowidth"
            fullWidth
            label="Companion"
            defaultValue=""
            onChange={chooseCompanion}
            value={selectedCompanion}
            error={selectedCompanion > 0 ? false : true}
          >

            {companions.map((companion, index) => (
              <MenuItem value={companion.id} key={index}>{companion.title}</MenuItem>
            ))}

          </Select>

          <br></br>


          <h1>Adjust Zones</h1>
          <p>Adjust the amount of questions per zone</p>
          <div class="zone-setting-container">
            <div>
              <p>Opener: </p>
            </div>
            <div>
              <Select
               onChange={(e) => setOpenersVal(e.target.value)}
               value={openersVal} 
               error={openersVal > 0 ? false : true}
               >
                <MenuItem value={1}>Low</MenuItem>
                <MenuItem value={2}>Medium</MenuItem>
                <MenuItem value={3}>High</MenuItem>
              </Select>
            </div>
          </div>
          <div class="zone-setting-container">
            <div>
              <p>Middle: </p>
            </div>
            <div>
            <Select
               onChange={(e) => setMiddleVal(e.target.value)}
               value={middleVal} 
               error={middleVal > 0 ? false : true}
               >
                <MenuItem value={3}>Low</MenuItem>
                <MenuItem value={4}>Medium</MenuItem>
                <MenuItem value={5}>High</MenuItem>
              </Select>
            </div>
          </div>
          <div class="zone-setting-container">
            <div>
              <p>Closer: </p>
            </div>
            <div>
            <Select
               onChange={(e) => setClosersVal(e.target.value)}
               value={closersVal} 
               error={closersVal > 0 ? false : true}
               >
                <MenuItem value={1}>Low</MenuItem>
                <MenuItem value={2}>Medium</MenuItem>
                <MenuItem value={3}>High</MenuItem>
              </Select>
            </div>
          </div>
          <br></br>
          <input type="submit" value="Set" className="btnSubmit" disabled={valid ? false : true}
          style={valid ? {cursor: 'pointer'} : {opacity: 0.5}}></input>
          <br></br>
          {
            (valid) ? 
            <p></p> :
            <p style={{color: 'red'}}>Please ensure all fields are selected</p>
          }
        </form>
        <br></br>
        <a href="/settings" id="signUp">Back To Settings Menu </a>
      </div>
    </div>
  )
}

export default InterviewSettings
