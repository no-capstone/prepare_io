import React, { useState } from 'react'
import logoImage from '../assets/images/logo.png';
import '../assets/styles/zoneSettings.css';
import axios from '../axios.js';


const AdjustZone = () => {

    const [openersVal, setOpenersVal] = useState(0)
    const [middleVal, setMiddleVal] = useState(0)
    const [closersVal, setClosersVal] = useState(0)

    const myStyle = {
        position: "relative",
        top: "50%",
        transform: "translateY(-50%)",
        width: "100%"
    }


    const handleSubmit = (e) => {
        e.preventDefault()

        console.log(openersVal)
        console.log(middleVal)
        console.log(closersVal)

        var openers = openersVal
        var middle = middleVal
        var closers = closersVal

        const zones = { openers, middle, closers }

        console.log(zones)

        axios.post(
            '/api/settings/adjustZones',
            {
                zones: zones
            }).then((res) => {
                console.log("RESP")
                console.log(res)
            })
    }

    return (
        <div>
            <div id="reg-page">
                <form onSubmit={handleSubmit}>
                    <img src={logoImage}></img>
                    <h1>Adjust Zones</h1>
                    <p>Adjust the amount of questions per zone</p>
                    <div class="zone-setting-container">
                        <div>
                            <p>Opener: </p>
                        </div>
                        <div>
                            <input type="number" min="0" max="10" style={myStyle} onChange={(e) => setOpenersVal(e.target.value)}></input>
                        </div>
                    </div>
                    <div class="zone-setting-container">
                        <div>
                            <p>Middle: </p>
                        </div>
                        <div>
                            <input type="number" min="0" max="10" style={myStyle} onChange={(e) => setMiddleVal(e.target.value)}></input>
                        </div>
                    </div>
                    <div class="zone-setting-container">
                        <div>
                            <p>Closer: </p>
                        </div>
                        <div>
                            <input type="number" min="0" max="10" style={myStyle} onChange={(e) => setClosersVal(e.target.value)}></input>
                        </div>
                    </div>
                    <br></br>
                    <input type="submit" value="Update" className="btnSubmit"></input>

                </form>
                <br></br>
                <a href="/settings" id="signUp">Back To Settings Menu </a>
            </div>
        </div>
    )
}

export default AdjustZone