import React, { useEffect, useState } from "react";
import '../assets/styles/conversation.css';
import Chat from '../components/chat/Chat';
import RecordButton from '../components/chat/RecordButton';
import Companion from '../components/chat/Companion';
import Button from '@material-ui/core/Button';
import axios from '../axios.js';

import { Link, useNavigate } from "react-router-dom";


var globalIndex = 0;
var fCap = []
var usedQuestions = {};
var failedQuestions = {};


var session_id = 0;


var companionId = 1;
var userId = 0;
var firstInit = true;

var handleMessages = [{msg: 'Could you try to answer again? It was hard to understand you', mood: 'neutral'},{ msg:'Sorry, my phone just called and I did not hear you properly. Can you try again?', mood: 'sadness'},{msg: 'Sorry...there may be an issue with my audio, should have not listened too much "silence", can you repeat that?', mood: 'positive'},{ msg:'It was hard to understand you, can you try again? Don`t be nervous, and try your best to speak clearly!', mood: 'neutral'}, {msg: 'Maybe it is better for you to start Stand-up comedy, you sure know how to break the silence..', mood: 'joy'},{msg:'I feel like it is waste of time for both of us, but going to give your last chance', mood: 'angry'},{msg:'Thank you for your presence and good luck in the future.', mood: 'disgust'}]
var handleCounter = 0;

class Conversation extends React.Component {


    constructor(props) {
        super(props);

        let defaultMsg = {
            id: 0,
            message: "Info: You have an official conversation with the Prepare.io member, to start communication, press the 'Start Interview' button and record appropriate answers to the messages from the computer. Remember to be polite and mindful of the topic. “Failure is instructive. The person who really thinks learns quite as much from his failures as from his successes.” ― John Dewey",
            type: "computer"
        };

        this.state = {
            humanMsgId: 0,
            companionInfo: {},
            mode: "interview",
            defaultMsg: defaultMsg,
            messages: [defaultMsg],
            isConversationStarted: false,
            conversationLabel: "Start Interview",
            companionId: 0,
            isMicDisabled: true,
            isFinished: false
        };

        this.getComputerMessage = this.getComputerMessage.bind(this)
        this.changeConversationState = this.changeConversationState.bind(this);
        this.handleNewMessage = this.handleNewMessage.bind(this);
        this.getComputerData = this.getComputerData.bind(this);
        this.fetchCurrentUserId = this.fetchCurrentUserId.bind(this);
        this.fetchSettings = this.fetchSettings.bind(this);

    }


    getHandleMessageCounter() {
        return (handleCounter != handleMessages.length - 1) ? handleCounter++ : handleCounter;
    }
    changeConversationState() {

        this.setState(prevState => ({
            isConversationStarted: !prevState.isConversationStarted,
            conversationLabel: !prevState.isConversationStarted ? "End Interview" : "Start Interview",
            messages: []
        }),

            function () {
                if (this.state.isConversationStarted) {
                    this.getComputerMessage();
                } else {


                    this.setState({
                        messages: [this.state.defaultMsg]
                    });

                }
            }
        );

    }
    fetchCurrentUserId = async () => {
        const ttoken = await localStorage.getItem('token');
        await axios.post(
            '/api/users/getCurrentUser',
            {
                token: ttoken
            }
        ).then((res) => {
            const id = res.data.currentUser.id
            userId = id;
            this.fetchSettings()
        })
    }

    fetchSettings = async () => {



        const updateFrequencyCap = (openers,middle,closers) => {


            // the most stupid things I wrote 
            while(true) {

                if(openers!=0) {
                    openers--
                    fCap.push("openers")
                    continue
                }

                if(middle!=0) {
                    middle--
                    fCap.push("middle")
                    continue
                }

                if(closers!=0) {
                    closers--
                    fCap.push("closers")
                    continue
                }


                break;

            }


        }


        const updateCompanions = (id) => {

            this.setState({
                companionId: id
            });

            const comapnionAlex = {
                "companionId": 1,
                "name": "Aleksei Kheba",
                "title": "IT Support Lead",
                "info": {
                    "appreciation": [
                        "Loves customer service",
                        "Looking for a candidate who can meet strict deadlines",
                        "Ablility to work in a fast paced environment"
                    ],
                    "job_place": "Liberty Mutual",
                    "profile_image": "aleksei.jpg"
                },
                "agenda": "You’ll join an organization that has spent its history anticipating the future. We aim to leverage our insights and experience to better preserve and protect what people earn, build, own, and cherish. ",
                "keywords": ["Ticketing", "Customer Service", "IT Support", "Jira", "BitBucket"]
            };

            const companionSean = {
                "companionId": 2,
                "name": "Sean Mapanao",
                "title": "Developer",
                "info": {
                    "appreciation": [
                        "Emphasis on customer service",
                        "Needs great communication skills",
                        "Looking for someone that is punctual"
                    ],
                    "job_place": "Buttu & Seif",
                    "profile_image": "sean.jpeg"
                },
                "agenda": "Our company is growing rapidly and is searching for experienced candidates for the position of backend developer. If you are looking for an exciting place to work.",
                "keywords": [".Net Core", "API", "development", "back-end", "Jira", "Web development", "communication skills"]
            };
            const companionRob = {
                "companionId": 4,
                "name": "Robert Chung",
                "title": "Front-end Developer",
                "info": {
                    "appreciation": [
                        "Loves UX/UI designs",
                        "Passionate about teaching",
                        "Loves working with eager and passionate engineers"
                    ],
                    "job_place": "RBC",
                    "profile_image": "rob.jpeg"
                },
                "agenda": "We're looking for a full-time front end developer who is interested in working on a wide range of web development projects in a laid back, friendly, and fun work environment.",
                "keywords": ["teaching", "UI", "designs", "front-end", "developer", "manager", "team oriented", "UX"]
            };
            const companionVin = {
                "companionId": 3,
                "name": "Vincent Irobun",
                "title": "AWS Cloud Developer",
                "info": {
                    "appreciation": [
                        "Loves learning new skills",
                        "Looking for interpersonal skills",
                        "Respects diligent and hardworking people"
                    ],
                    "job_place": "CGI Group",
                    "profile_image": "vincent.jpeg"
                },
                "agenda": "Our growing company is looking to fill the role of AWS cloud engineer to join our growing team. ",
                "keywords": ["Programming", "Problem-solving", "Enthusiastic", "Proactive", "Web fundamentals"]
            };

            let allCompanions = {
                1: comapnionAlex,
                2: companionSean,
                3: companionVin,
                4: companionRob
            }



            this.addCompanion(allCompanions[id]);

        }


        await axios.post('/api/settings/fetchSettings',
            {
                userId: userId
            }
        ).then((res) => {
            companionId = res.data.companionId;
            updateCompanions(companionId)
            updateFrequencyCap(res.data.openers,res.data.middle,  res.data.closers)
        })
    }

    componentDidMount() {
        this.fetchCurrentUserId();
    }

    addComputerMessage(msgObj = {}) {
        msgObj.type = 'computer';
        if (msgObj.id < 1000 && !firstInit) {
            usedQuestions[msgObj.id] = false;
        }


        this.setState({
            isMicDisabled: true
        });
        var voice = new SpeechSynthesisUtterance(msgObj.message);


        // test on 1.5, default is 0.9 
        voice.rate = 0.9
        
        voice.addEventListener('end', (event) => {
            this.setState({
                isMicDisabled: false
            });
          });        



        setTimeout(function() {
            window.speechSynthesis.speak(voice);
        },1000)  
        


        this.setState({
            messages: [...this.state.messages, msgObj]
        });
    }

    addCompanion(companion) {

        this.setState({
            companionInfo: companion
        });
    }




    handleNewMessage(msg = {}) {

        let humanMsg = {
            id: "h-" + (this.state.humanMsgId + 1),
            message: "...",
            type: "human"
        };

        this.setState((prev) => ({
            humanMsgId: prev.humanMsgId + 1,
            messages: [...this.state.messages, humanMsg]
        })
        );

    }

    getComputerData(val) {

        const { status, data } = val.data ?? val;

        if (status) {

            // let id = data.question_id ? data.question_id : this.state.humanMsgId+1000;

            if (data.computerResponse != '') {
                // change it tomorrow 2022/09/27 (never)
                let computerStructure = {
                    id: this.state.humanMsgId + 1000,
                    message: data.computerResponse,
                    mood: data.mood,
                    zone: fCap[globalIndex]
                };

                this.addComputerMessage(computerStructure);
            }

            if (Object.keys(data.followUpQuestion).length !== 0) {

                let followUpStructure = {
                    id: data.followUpQuestion.id,
                    message: data.followUpQuestion.text,
                    zone: fCap[globalIndex]
                };

                this.addComputerMessage(followUpStructure);
            } else {
                // getting more questions
                this.getComputerMessage();
            }

        } else {

            let msgResponse = handleMessages[this.getHandleMessageCounter()];
            let computerStructure = {
                id: Math.random() + 10000,
                message: msgResponse.msg,
                mood: msgResponse.mood,
                zone: fCap[globalIndex],
                askedQuestion: val.data.askedQuestion
            };

            this.addComputerMessage(computerStructure);
            // do something
        }



    }

    // get messages
    // http://localhost:8080/api/questions/get?user_id=0&zone=intro&session_start=true
    getComputerMessage(type = "openers",repeatQuestion = false) {
        if (!firstInit) {
            usedQuestions[1] = 0;
        }
        let questArr = Object.keys(usedQuestions);

        type = fCap[globalIndex];

        if(typeof type == 'undefined') {
            this.setState({isFinished: true})
            this.setState({
                isMicDisabled: true
            });

            let endResponse = {
                id: Math.random() + 10000,
                message: 'It was a pleasure to talk to you. Click on the "Calculate Score" button to see your probability of success.',
                mood: 'neutral'
            };
            this.addComputerMessage(endResponse);

            return
        } 

        let usedQuestionsStr = questArr.length != 0 ? questArr.join() : '';
        let additionalParams = firstInit ? '&session_start=true' : '';
        additionalParams += '&used_questions=' + usedQuestionsStr;
        additionalParams +=  '&companionId=' + this.state.companionId;

        axios.get(`/api/questions/get?user_id=0&zone=${type}` + additionalParams)
            .then(res => {

                res.data.data.zone = fCap[globalIndex]
                if (res.data.status) {

                    this.addComputerMessage(res.data.data);

                    if (firstInit) {
                        session_id = res.data.data.session_id;
                    }

                } else {

                    this.addComputerMessage(res.data.data);
                    // make a valdiation
                }
                firstInit = false;
                globalIndex++;
            });



    }


    render() {
        const set = session_id
        const isStarted = this.state.isConversationStarted;
        const conversationLabel = this.state.conversationLabel;

        return (

            <div id="conversation-page">

                <div id='header-text'>
                    <h2>Interview Mode</h2>
                    <p>With a little preparation, you can be ready for anything.</p>
                </div>

                <div className='flex-container'>

                    <div className="opponent-container smooth-box">
                        <Companion info={this.state.companionInfo} />
                    </div>
                    <div className="chat-container">
                        <div className='smooth-box'>
                            <h3 className="sub-text">Aleksei on the Line</h3>
                            <Chat messages={this.state.messages} />

                        </div>
                        <div id="controls-container">

                            <Button id="conversationToggleBtn" variant="contained" color="secondary" onClick={this.changeConversationState}
                            //onClick={this.getComputerMessage} 
                            >{conversationLabel}</Button>
                            {isStarted ? <RecordButton session_id={session_id} msgId={this.state.humanMsgId} sendData={this.getComputerData} newMessage={this.handleNewMessage} usedQuestions={usedQuestions}  companionId={this.state.companionId} isMicDisabled={this.state.isMicDisabled} /> : null}
                            {this.state.isFinished ? <Button color="default" /*onClick={getSess()}*/><Link to="/score" state={{ id: session_id }}>Calculate Score</Link></Button> : null}
                        </div>
                    </div>

                </div>

            </div>

        );
    }

}


export default Conversation;
export const data = session_id;