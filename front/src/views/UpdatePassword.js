import React from 'react'
import logoImage from '../assets/images/logo.png';
import '../assets/styles/register.css';
import { TextField } from '@material-ui/core';
import { useState, useEffect } from 'react';
import axios from '../axios.js';
import { useNavigate } from 'react-router-dom'

const UpdatePassword = () => {

  const [newPw, setNewPw] = useState('')

  const [confirmPw, setConfirmPw] = useState('')

  const [oldPw, setOldPw] = useState('')

  const [email, setEmail] = useState('')

  const [showElement,setShowElement] = useState(false)

  const [valid, setValid] = useState(false)

  const navigate = useNavigate()

  const validate = () => {
    if((confirmPw === newPw) && (newPw.length > 5) && (oldPw.length > 5)){
      setValid(true)
    } else {
      setValid(false)
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log(newPw)

    console.log("AFTER POST REQUEST")
    console.log(email)
    axios.put(
      'api/settings/updatePassword', 
      {
      email: email,  
      password: newPw,
      oldPassword: oldPw
      },
  
    ).then((res)=> {
      navigate("/updatePassword")
      console.log(res)
    })

    setShowElement(true)
    e.target.reset();
    setTimeout(function() {
      setShowElement(false)
         }, 3000);
  }


  useEffect(() => {
 
    const ttoken = localStorage.getItem('token');

    axios.post(
      '/api/users/getCurrentUser',
      {
      token: ttoken
      }
      ).then((res) => {
        const eemail = JSON.stringify(res.data.currentUser.email)
        setEmail(eemail)
      })

      validate()
  }, [email, valid,newPw, confirmPw, oldPw])

  return (
    <div>
      <div id="reg-page">
        <form onSubmit={handleSubmit}>
          <img src={logoImage}></img>
          <h1>Update Password</h1>
          <TextField id="standard-basic" label="Current Password" fullWidth required onChange={(e) => setOldPw(e.target.value)} type="password"/>
          <br></br>
          <TextField id="standard-basic" label="New Password" fullWidth required variant="standard" type="password" helperText={newPw.length != 0 && newPw.length < 6 ? "New Password Must Be At Least 6 Characters" : ""}
            error={newPw.length < 6 && newPw.length > 0 ? true : false} onChange={(e) => setNewPw(e.target.value)} />
          <br></br>
          <TextField id="standard-basic" label="Confirm Password" fullWidth required variant="standard"  helperText={confirmPw === newPw ? "" : "New Passwords Must Match"}
            error={confirmPw === newPw ? false : true} onChange={(e) => setConfirmPw(e.target.value)} type="password"/>
          <br></br>
          <br></br>
          <input type="submit" value="Update" className="btnSubmit" disabled={valid ? false : true}  style={valid ? {cursor: 'pointer'} : {opacity: 0.5}}></input>
          <br />
          {
            (showElement) ? 
            <p>Password Successfully Changed!</p> :
            <p></p>
          }
        </form>
        <br></br>
        <a href="/settings" id="signUp">Back To Settings Menu </a>
      </div>
    </div>
  )
}

export default UpdatePassword

// <input type="submit" value="Set" className="btnSubmit" disabled={valid ? false : true}
//           style={valid ? {cursor: 'pointer'} : {opacity: 0.5}}></input>

