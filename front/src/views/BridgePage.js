import React from 'react'
import { useState, useEffect } from 'react'
import '../assets/styles/bridgePage.css';
import imgRob from '../assets/images/rob.jpeg';
import imgAleksei from '../assets/images/aleksei.jpg';
import imgSean from '../assets/images/sean.jpeg';
import imgVincent from '../assets/images/vincent.jpeg';
import axios from '../axios.js';
import { SidebarData } from '../components/dashboard/SidebarData'
import Logo from "../assets/images/logo.png";
import imgJos from "../assets/images/jos.jpeg"
// import Sidebar from '../components/dashboard/Sidebar';

const BridgePage = () => {
  const [companionName, setCompanionName] = useState("")
  const [companionId, setCompanionId] = useState(0)
  const [companionTitle, setCompanionTitle] = useState("")
  const [companionJobPlace, setCompanionJobPlace] = useState("")
  const [companionAgenda, setCompanionAgenda] = useState("")
  const [userId, setUserId] = useState(0)
  const [companionImg, setCompanionImg] = useState("")
  const [openersZone, setOpenersZone] = useState(0)
  const [middleZone, setMiddleZone] = useState(0)
  const [closersZone, setClosersZone] = useState(0)
  const [question, setQuestion] = useState("")
  const [tipId, setTipId] = useState(0)
  const [tip, setTip] = useState("")
  const [average, setAverage] = useState(89)
  const [highScore, setHighScore] = useState(100)
  const [users, setUsers] = useState(9)
  const [interviews, setInterviews] = useState(88)
  const fetchQuestion = async () => {
    axios.get(
      '/api/questions/getRandomQuestion'
    ).then((res) => {
      console.log(res)

      setQuestion(res.data.questions.text)
      setTipId(res.data.questions.tip_id)
      console.log("LOOK HEREEEE")
      console.log(tipId)
      console.log(res.data.questions.tip_id);
      //fetchTip()
       axios.post('/api/questions/getTip',
      {
        tipId: res.data.questions.tip_id
      }
    ).then((res) => {
      console.log(res)
      let tips = res.data.tips
      console.log(tips)
      setTip(tips)
      console.log("Here's a tip: " + tips)
    }).catch((err) => {
      console.log(err)
    })

    })
  }

  // const fetchTip = async () => {
  //   console.log("helloooooo " + tipId)
  //   await axios.post('/api/questions/getTip',
  //     {
  //       tipId: tipId
  //     }
  //   ).then((res) => {
  //     console.log(res)
  //     let tips = res.data.tips
  //     console.log(tips)
  //     setTip(tips)
  //     console.log("Here's a tip: " + tips)
  //   }).catch((err) => {
  //     console.log(err)
  //   })
  // }

  const fetchUserStats = async() => {
    console.log("Getting settings.." + userId);
    
    await axios.post('/api/stats/userStats', {
      userId: userId
    }).then((res) =>{
      console.log(res);

      setHighScore(res.data.high)
      setAverage(res.data.average)
    })
  }

  const fetchGlobalStats = async () => {
    console.log("Fetching global statistics...");

    await axios.get('/api/stats/globalStats')
    .then((res)=> {
      console.log("Here you go: " + res);

      setInterviews(res.data.total)
      setUsers(res.data.userCount)
    })
  }

  const fetchCurrentUserId = async () => {
    const ttoken = await localStorage.getItem('token');
    await axios.post(
      '/api/users/getCurrentUser',
      {
        token: ttoken
      }
    ).then((res) => {
      const id = res.data.currentUser.id
      setUserId(id)
      console.log("id: " + userId)
      fetchSettings()
    })
  }

  const fetchCompanionInfo = async () => {
    await axios.post('/api/companions/getAllCompanionsInfo', {
      companionId: companionId
    }).then((res) => {
      setCompanionAgenda(res.data.agenda)
      setCompanionName(res.data.compName)
      setCompanionTitle(res.data.compTitle)
      setCompanionJobPlace(res.data.compJobPlace)
      setCompanionImg(res.data.compImg)

      const image = res.data.compImg

      switch (image) {
        case 'rob.jpg':
          setCompanionImg(imgRob)
          break;
        case 'aleksei.jpg': setCompanionImg(imgAleksei)
          break;
        case 'sean.jpg': setCompanionImg(imgSean)
          break;
        case 'vincent.jpg': setCompanionImg(imgVincent);

      }

    })
  }

  const fetchSettings = async () => {
    await axios.post('/api/settings/fetchSettings',
      {
        userId: userId
      }
    ).then((res) => {
      setCompanionId(res.data.companionId)
      setOpenersZone(res.data.openers)
      setMiddleZone(res.data.middle)
      setClosersZone(res.data.closers)
      fetchCompanionInfo()
      console.log("com id: " + companionId)
    })
  }


  useEffect(() => {
    fetchCurrentUserId()
    fetchQuestion()
    fetchGlobalStats()
    fetchUserStats()

  }, [userId, companionId, openersZone]);


  return (
    
    <div id="main">
      {/* <div className='Sidebar'>
        <img src={Logo} id="logo" />
        <ul className='SidebarList'>
          {SidebarData.map((val, key) => {
            return (
              <li key={key}
                className="row"
                onClick={() => { window.location.pathname = val.link }}>
                {" "}
                <div>{val.icon}</div>{" "}
                <div>
                  {val.title}
                </div>
              </li>
            )
          })}
        </ul>
      </div> */}
      <div id="title">
      <h1>Hi, Welcome Back</h1>
      </div>
      
      {/* <div class = "SideStats"> */}
        <div class="cardInfoContainer">
          <h3>{average}</h3>
          <h3>Total Average Score</h3>
        </div>
        <br></br>
        <br></br>
        <div class="cardInfoContainer2">
          <h3>{highScore}</h3>
          <h3>Highest Interview Score</h3>
        </div>
        <br></br>
        <br></br>
        <div class="cardInfoContainer3">
          <h3>{users}</h3>
          <h3>Global Users</h3>
        </div>
        <br></br>
        <br></br>
        <div class="cardInfoContainer4">
          <h3>{interviews}</h3>
          <h3>Interviews Done Through System</h3>
        </div>
      {/* </div> */}
      
      <div id="question" >
        <h2>Random Question</h2>
        <p>{question}</p>
        <p><b>TIP: </b>{tip}</p>
      </div>
      
      <br></br>
        
        <div class="cardContainer1">
          <h2>Your Interviewer</h2>
          <div id="imgWrapper">
            <img src={companionImg} alt="companion image"></img>
          </div>
          <h3>{companionName}</h3>
          <p>{companionTitle}</p>
          <p><em>{companionJobPlace}</em></p>
          <p>{companionAgenda}</p>
        </div>
        <div class="cardContainer2">
          <h2>Your Conversation Configuration</h2>
          <div id="zones">
            <p>Openers:     {openersZone}</p>
            <p>Middle:     {middleZone}</p>
            <p>Closers:     {closersZone}</p>
          </div>
        </div>
        

      <div id="buttons">
        <div>
          <a href="/menu" id="proceed">Begin Interviewing!</a>
        </div>

      </div>
      

        <div class="cardContainerSponsor">
          <h2>Sponsored Companion</h2>
          <div id="imgWrapper">
            <img src={imgJos} alt="companion image"></img>
          </div>
          <h3>Jos Stanton</h3>
          <p>CEO</p>
          <p><em>Christyanton Inc</em></p>
          <p>You'll be working with other software developers to take product ideas from concept to release. You'll
            operate at all levels of the stack. Create front-end code that drives the user interface/experience to backend business logic and architecture. Lastly,
            Design, develop, test, document, and deploy new software products & features</p>
        </div>
        
    </div>
    // <div className='App'>
    //   <div className='main'>
    //   <h1>Hi, Welcome Back</h1>
    //   </div>
    // </div>
  )
}

export default BridgePage
