import React from "react"
import NavigationBar from "../components/base/Navbar"
import { Outlet } from "react-router-dom";
import '../assets/styles/base/layout.css';


const Layout = () => {
  return (
    <>
    <NavigationBar />
    <Outlet />
    </>
  )
};

export default Layout;