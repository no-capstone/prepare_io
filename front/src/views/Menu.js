import React from "react";
import '../assets/styles/menu.css';
import Modebar from "../components/Modebar"
import menuImage from '../assets/images/menu-image.jpeg';


class Menu extends React.Component {
    render() {
        return (
            <>
                <div className="title">
                    <h1>Let's Begin!</h1>
                    <h4>Prepare for your interview</h4>
                    <img src={menuImage}></img>
                </div>
                <Modebar />
                <div className="settings">
                    <a href="/settings">Settings</a>
                </div>
            </>
        );
    }
}
export default Menu;





