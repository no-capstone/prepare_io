import React, { useEffect, useState } from 'react'
import '../assets/styles/register.css';
import logoImage from '../assets/images/logo.png';
import axios from '../axios.js';
import { TextField } from '@material-ui/core';
import { useNavigate } from 'react-router-dom'



const email_regex = new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)
const pwLengthRestriction = 6

const Registration = () => {

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [firstName, setFName] = useState("")
  const [lastName, setLName] = useState("")
  const [isValidPassword, setPasswordValidity] = useState(false)
  const [isValidEmail, setEmailValidity] = useState(false)
  const navigate = useNavigate()


  useEffect(() => {
    if (password.length >= pwLengthRestriction) {
      setPasswordValidity(true)
    } else {
      setPasswordValidity(false)
    }
    if (email_regex.test(email)) {
      setEmailValidity(true)
    } else {
      setEmailValidity(false)
    }
  }, [password, email])



  const handleSubmit = (event) => {
    event.preventDefault()

    if ((isValidEmail) && (isValidPassword)) {
      axios.post(
        'api/users/register',
        {
          email: email,
          password: password,
          fName: firstName,
          lName: lastName
        },
      ).then((res) => {
        console.log(res.data)
        console.log(res.data.status)
        navigate("/login")
      }).catch((error) => {
        if (error.response) {
          console.log(error.response.data)
        }
      })
    }
  }

  return (
    <div>
      <div id="reg-page">
        <div>
          <form onSubmit={handleSubmit}>

            <img src={logoImage}></img>
            <h1>Register</h1>
            <TextField id="standard-basic" label="First Name" fullWidth required onChange={(e) => setFName(e.target.value)}/>
            <br></br>
            <TextField id="standard-basic" label="Last Name" fullWidth required onChange={(e) => setLName(e.target.value)}/>
            <br></br>
            <TextField id="standard-basic" label="Email" fullWidth 
            error={!isValidEmail} helperText={!isValidEmail ? "Must be a valid email address!" : ""}
            onChange={(e) => setEmail(e.target.value)}/>
            <br></br>
            <TextField id="standard-basic" label="Password" fullWidth 
            error={!isValidPassword} helperText={!isValidPassword ? "Password must be 6 characters minimum!" : ""}
            onChange={(e) => setPassword(e.target.value)}
            />

            <br /><br />
            <input type="submit" value="Submit" className="btnSubmit"></input>
            <br />
          </form>
          <p>Already have an account? <a href="/login" id="signUp">Login!</a></p>
        </div>
      </div>
    </div>
  )
}

export default Registration
