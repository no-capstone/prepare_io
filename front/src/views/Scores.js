import React, { useMemo } from 'react'
import '../assets/styles/scores.css';
import { useEffect, useState } from 'react';
import axios from '../axios.js';
import {data} from './Conversation'
import { useLocation } from 'react-router-dom';
import { Paper, Button, Popover, Typography, Divider } from '@material-ui/core';


const Scores = (props) => {
    const [scores, setScores] = useState([])
    const [tips, setTips] = useState([])
    const [quests, setQuests] = useState([])
   const [avg, setAvg] = useState(0)
    const location = useLocation();

    const [anchorEl, setAnchorEl] = useState(null);
    const [popOverId, setPopOverId] = useState(null);

    // Event handler for Button Popover
    // PopOverId is used to display the appropriate tip
    const handleOpen = (event, popId) => {
        setPopOverId(popId)
        setAnchorEl(event.currentTarget);
      };
    
    const handleClose = () => {
    setPopOverId(null)
    setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;
    

    console.log(location);
    
    const bringScores = async()=> {
        console.log("Session ID: ", location.state.id);
        axios.get(
            `/api/scores/get?session_id=${location.state.id}`
        ).then((res)=>{
            const sc = res.data.score
            const tip = res.data.tip
            const quests = res.data.question
            const avg = Math.round(res.data.average)
           
            setAvg(avg)
            setScores(sc)
            setQuests(quests)
            setTips(tip)
            
        })

    }

    useEffect(() => {
        bringScores();
    },[]

    )

    return (
        <div>
            <Typography variant="h1">Summary</Typography>
            <table>

                <tbody>
                    <tr>
                        <th><Typography variant="h5">Questions</Typography></th>
                        <th><Typography variant="h5">Scores</Typography></th>
                    </tr>
                    {quests.map((item1, i) => (
                        
                        <tr key={i}>
                            
                            <td><Typography variant="body1"> 
                                {item1} 
                            </Typography></td>
                            <td><Typography variant="body1" style={{fontSize: "20px"}}> 
                                {Math.round(scores[i])} 
                            </Typography></td>
                            
                            {scores[i] < 65 ? 
                            <td className="popover">
                                <Button aria-describedby={id} 
                                variant="contained" 
                                onClick={(e) => handleOpen(e, i)}>
                                    Tip
                                </Button>
                                <Popover
                                    id={id}
                                    open={open}
                                    anchorEl={anchorEl}
                                    onClose={handleClose}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    PaperProps={{
                                        style: {
                                            border: "solid #f9a034 2px",
                                            maxWidth: "30em"
                                        }
                                    }}>
                                    <Typography>{tips[popOverId]}</Typography>
                                </Popover>
                                
                            </td>
                             : undefined}
                        </tr>
                        )   
                    )}
                </tbody>
                
            </table>

            <Paper elevation={5} id="scoreDisplay">
                <span style={{ marginRight: "0.5em" }}>Overall Score:</span> <span className="overallScoreValue">{avg}</span>
            </Paper>

            {/* <h2>Tips:</h2>
            
             {quests.map((item3, r) => (
                <div key={r}>
                <p style={{ fontStyle: "italic", fontWeight: "bold" }}>{item3}</p>
                <p>{tips[r]}</p>
                </div>
            ))} */}
             
             <div id="buttons">
        <div>
          <a href="/menu" id="proceed">Begin Interviewing!</a>
        </div>

      </div>
        </div>
        
    )
}

export default Scores

