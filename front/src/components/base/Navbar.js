import React from "react";
import { NavLink } from "react-router-dom";
import '../../assets/styles/base/navbar.css';

import Logo from "./Logo";

// Render the NavLink for Menu only when logged in (token exists)
function Private ({shouldRender = false, children}) {
  return shouldRender ? children : null
}

// Clear the token and re-render the NavigationBar upon logging out
function logout() {
  localStorage.clear()
  NavigationBar.render()
}


class NavigationBar extends React.Component {
  render() {
    var token = localStorage.getItem('token')
    if (token != null) {
      var renderThis = true
    }

    return(

        <header className="nav">
           <Logo/>
            <nav className="modeButtons">
                <NavLink className={(navData) => (navData.isActive ? "selected_page" : 'none')} to="/home">Home</NavLink>
                <Private shouldRender={renderThis}>
                  <NavLink className={(navData) => (navData.isActive ? "selected_page" : 'none')} to="/menu">Menu</NavLink>
                  <NavLink className={(navData) => (navData.isActive ? "selected_page" : 'none')} to="/settings">Settings</NavLink>
                </Private>
                <NavLink className={(navData) => (navData.isActive ? "selected_page" : 'none')} to="/about">About</NavLink>
                {token === null ? 
                <NavLink className={(navData) => (navData.isActive ? "selected_page" : 'none')} to="/login">Login</NavLink>
                :
                <NavLink className={(navData) => (navData.isActive ? "selected_page" : 'none')} to="/home" onClick={logout}>Logout</NavLink>}

            </nav>
        </header>
    );
  }
}
export default NavigationBar;