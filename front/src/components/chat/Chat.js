import React, { Component } from 'react';
import Message from './Message';
import '../../assets/styles/chat.css';

class Chat extends Component {


  render() {

    return this.props.messages.map((msg,index) => (
      
      <Message index={index} key={msg.id} message={msg} />
    ));
  }
}

export default Chat;