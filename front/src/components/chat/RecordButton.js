import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import axios from '../../axios.js';
// import MicIcon from '@mui/icons-material/Mic';

const SpeechRecognition =
    window.SpeechRecognition || window.webkitSpeechRecognition
const mic = new SpeechRecognition()

mic.continuous = true
mic.interimResults = true
mic.lang = 'en-US'

var currentId = 0;

const RecordButton = (props) => {


    let isMicDisabled = props.isMicDisabled;



    const [isMicOn, setIsMicOn] = useState(false);

    currentId = "msg-id-h-"+props.msgId;

    var buttonColour;
    var buttonLabel;

    if (isMicOn) {
        buttonColour = "secondary";
        buttonLabel = "Recording...";
    } else {
        buttonColour = "primary";
        buttonLabel = "Record";
    }

    useEffect(() => {
        handleListen()
    }, [isMicOn])

    const handleListen = () => {
        if (isMicOn) {
            mic.start()
            props.newMessage();
            mic.onend = () => {
                console.log('continue..')
                mic.start()
            }
            
        } else {
            mic.stop()
            mic.onend = () => {


                let textString = document.getElementById(currentId).innerText;

                let allCompElements = document.querySelectorAll('.message.computer'),allElementsLen = allCompElements.length,lastElement = null;


                let counter = allElementsLen-1;
                lastElement = allCompElements[counter]

                let usedQuestionsStr = Object.keys(props.usedQuestions).join();

                let question_id = 0;

                if(lastElement.getAttribute('askedquestion')) {
                    question_id = parseInt(lastElement.getAttribute('askedquestion'))
                } else {
                    let question_id_arr = lastElement.getAttribute('id').split('-');
                    question_id = question_id_arr[2] || 1;
                }
                

                let type = lastElement.getAttribute('zone')
                
                // update with real user id later ( never )
                // make post later (never)
                axios.get(`/api/questions/calculate?user_id=0&zone=${type}&question_id=${question_id}&user_answer=${textString}&used_questions=${usedQuestionsStr}&session_id=${props.session_id}&companionId=${props.companionId}`)
                .then(res => {
                    props.sendData(res);
        
                }).catch(e => {

                    if(typeof e.status != 'undefined' ) {

                        let response = {status: false, data: {msg: e.msg}}
                        if(e.askedQuestion) {
                            response.data.askedQuestion = e.askedQuestion
                        }
                        
                        props.sendData(response);
                    }

                    
                    

                });

// fetch('http://localhost:3000/message/info', requestOptions)
//     .then(response => response.json())
//     .then(data => {

//         props.sendData(data);
//         console.log(data);
//  });

                console.log('Stopped Mic on Click')
            }
        }
        mic.onstart = () => {
            console.log('Mics on')
        }

        mic.onresult = event => {
            const transcript = Array.from(event.results)
                .map(result => result[0])
                .map(result => result.transcript)
                .join('')


            document.getElementById(currentId).innerText = transcript;

            mic.onerror = event => {
                console.log(event.error)
            }
        }
    }
    // if you don't want to us the button from Material-ui, just change Button to button
    return (
        <Button disabled={isMicDisabled} className="recording-button" variant="contained" color={buttonColour} onClick={() => { setIsMicOn(!isMicOn) }} >{buttonLabel}</Button>
    )
}

export default RecordButton;