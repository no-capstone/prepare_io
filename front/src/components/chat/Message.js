import React from "react";
// import '../assets/styles/conversation.css';
// import personImage from '../assets/images/person-image.png';


const icons = {
    positive : '🙂',
    joy : '😃',
    neutral : '🤔',
    anger : '😠',
    fear : '😨',
    sadness : '😥',
    disgust : '😞'

}

class Message extends React.Component {
    render() {

        const { id,message,type,mood,askedQuestion,zone } = this.props.message;
        return (
            <p id={'msg-id-'+id} orderid={this.props.index} {...(zone ? {zone: zone} : '')} {...(askedQuestion ? {askedquestion: askedQuestion} : '')} className={"message " + type}> { type == 'computer' && icons[mood] ? icons[mood] + ' ' : ''}{message}</p>
        )
    }
}


export default Message;