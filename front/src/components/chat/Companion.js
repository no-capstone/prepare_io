import React from "react";
import '../../assets/styles/companion.css';
// import personImage from '../../assets/images/aleksei.jpg';
// import LandpageImage from '../assets/images/landpage-image.jpeg';



class Companion extends React.Component {


  render() {


    const companion = this.props.info;


    const {name, title, agenda, info} = companion;
    
    return (
      <>
      
      <div className="companion-card-background"></div>
      <div id="companion-info">
      <img id="companion-image" width="150px" src={ info ? require('../../assets/images/'+info.profile_image) : "" }></img>
      <h3>{name ? name : "..."}</h3>
      <h4>{title ? title : "..."}</h4>

      <hr></hr>
      <h4>Personal Info</h4>
      {info ? info.appreciation.map((value, index) => {
        return <li key={index}>{value}</li>
      }) : null}
            <hr></hr>
      <h4>Agenda</h4>
      <p>{agenda}</p>
      </div>




        
      </>
    );
  }
}
export default Companion;
