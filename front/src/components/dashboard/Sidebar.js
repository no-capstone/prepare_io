import { ListItem } from '@material-ui/core'
import React from 'react'
import "../../assets/styles/bridgePage.css"
import { SidebarData } from './SidebarData'
import Logo from "../../assets/images/logo.png";

function Sidebar() {
  return (
    <div className='Sidebar'>
      <img src={Logo} id="logo"/>
        <ul className='SidebarList'>
      {SidebarData.map((val,key)=>{
        return(
            <li key={key} 
            className="row"
            onClick={()=>{window.location.pathname = val.link}}>
                {" "}
                <div>{val.icon}</div>{" "}
                <div>
                    {val.title}
                </div>
            </li>
        )
      })}
      </ul>
    </div>
  )
}

export default Sidebar
