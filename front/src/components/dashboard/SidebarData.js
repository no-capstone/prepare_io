import React from 'react'
// import HomeIcon from '@mui/icons-material/Home';
// import MenuIcon from '@mui/icons-material/Menu';
// import SettingsIcon from '@mui/icons-material/Settings';
// import InfoIcon from '@mui/icons-material/Info';
// import alekseiImage from '../assets/images/aleksei.jpg';


export const SidebarData = [
    {
        title: "Home",
        // icon: <HomeIcon/>,
        link: "/home"
    },
    {
        title: "Menu",
        // icon: <MenuIcon/>,
        link: "/menu"
    },
    {
        title: "About",
        // icon: <InfoIcon/>,
        link: "/about"
    },
    {
        title: "Settings",
        // icon: <SettingsIcon/>,
        link: "/settings"
    }
]