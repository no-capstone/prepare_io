import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./views/Layout";
import Home from "./views/Home";
import Menu from "./views/Menu";
import Conversation from "./views/Conversation";
import Login from "./views/Login";
import About from "./views/About";
import Registration from "./views/Registration";
import Scores from "./views/Scores";
import ProtectedRoute from "./ProtectedRoute";
import Settings from "./views/Settings";
import UpdatePassword from "./views/UpdatePassword";
import InterviewSettings from "./views/InterviewSettings";
import BridgePage from "./views/BridgePage";


export default function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="/home" element={<Home/>} />
          <Route path="/about" element={<About />} />
          <Route path="/login" element={<Login/>} />
          <Route path="/registration" element={<Registration />} />
          <Route path="/score" element={<Scores />} />
          <Route element={<ProtectedRoute/>}>
            <Route path="/menu" element={<Menu />} />
            <Route path="/settings" element={<Settings/>}/>
            <Route path="/updatepassword" element={<UpdatePassword/>} />
            <Route path="/interviewSettings" element={<InterviewSettings/>}/>
            <Route path="/conversation" element={<Conversation/>} />
            <Route path="/bridgePage" element={<BridgePage/>} />
          </Route>
          <Route path="*" element={<Home />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));