const mongoose = require('mongoose')

const streamTypeSchema = mongoose.Schema(
    {
        id: {
            type: Number,
            required: [true, ''],
        },
        type: {
            type: String,
            required: [true, ''],
        },
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('streamType', streamTypeSchema)


// add later
//emotions [],
//sentiment