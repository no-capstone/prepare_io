const mongoose = require('mongoose')

const tipSchema = mongoose.Schema(
    {
        id: {
            type: Number,
            required: [true, ''],
        },
        tips: {
            type: String,
            required: [true, ''],
        },
        
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('tips', tipSchema)