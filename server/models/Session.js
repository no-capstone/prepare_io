const mongoose = require('mongoose')

const sessionSchema = mongoose.Schema(
    {
        id: {
            type: Number,
            required: [true, ''],
        },
        user_id: {
            type: Number,
            required: [true, ''],
        }
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('session', sessionSchema)