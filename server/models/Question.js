const mongoose = require('mongoose')

const questionSchema = mongoose.Schema(
    {
        id: {
            type: Number,
            required: [true, ''],
        },
        type_id: {
            type: Number,
            required: [true, ''],
        },
        comp_id:{
            type: [Number],
            required: [true, ''],
        },
        tip_id: {
            type: Number,
            required: [true, ' '],
        },
        answer_id: {
            type: Number,
            required: [true, ' '],
        },
        category_id: {
            type: [Number],
            required: [true, ' '],
        },
        keywords: {
            type: [String],
            required: [true, ''],
            unique: true,
        },
        zone: {
            type: String,
            required: [true, ''],
        },
        text: {
            type: String,
            required: [true, ''],
        },
    },
    {
        methods: {

            findByKeywords(keywords = []) {
                return "shees";
            } 
        },
        timestamps: true,
    }
)


module.exports = mongoose.model('Question', questionSchema)