const mongoose = require('mongoose')

const answerSchema = mongoose.Schema(
    {
        id: {
            type: Number,
            required: [true, ''],
        },
        keywords: {
            type: [String],
            required: [true, ''],
            unique: false,
        },
        good: {
            type: String,
            required: [true, ''],
        },
        bad: {
            type: String,
            required: [false, ''],
        },
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('answers', answerSchema)