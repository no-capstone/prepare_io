const mongoose = require('mongoose')

const companionSchema = mongoose.Schema(
    {
        id: {
            type: Number,
            required: [true, ''],
        },
        name: {
            type: String,
            required: [true, ''],
        },
        title: {
            type: String,
            required: [true, ''],
        },
        info: {
            appreciation: {
                type: [String],
                required: [true, ''],
                unique: false,
            },
            job_place: {
                type: String,
                required: [false, ''],
            },
            profileImg: {
                type: String,
                required: [false, ''],
            },
            type: Object,
            required: [false, ''],
        },
        agenda: {
            type: String,
            required: [false, ''],
        },
        keywords: {
            type: [String],
            required: [true, ''],
            unique: false,
        },
        
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('companions', companionSchema)