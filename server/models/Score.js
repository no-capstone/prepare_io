const mongoose = require('mongoose')

const scoreSchema = mongoose.Schema(
    {
        id: {
            type: Number,
            required: [true, ''],
        },
        quest_id: {
            type: Number,
            required: [true, ''],
        },
        session_id: {
            type: Number,
            required: [true, ''],
        },
        score_p: {
            type: Number,
            required: [true, ''],
        },
        user_answer: {
            type: String,
            required: [true, ''],
        },
        
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('score', scoreSchema)


// add later
//emotions [],
//sentiment