const mongoose = require('mongoose')

const settingsSchema = mongoose.Schema(
    {
        id: {
            type: Number,
            required: [true, ''],
        },
        user_id: {
            type: Number,
            required: [true, '']
        },
        companion_id: {
            type: Number,
            required: [true, ''],
        },
        category_id: {
            type: [Number],
            required: [true, ''],
        },
        type_id:{
            type: Number,
            required: [true, '']
        },
        zone_length:
        {
            openers: {
                type: Number,
                required: [true, ''],
            },
            middle: {
                type: Number,
                required: [true, ''],
            },
            closers: {
                type: Number,
                required: [true, ''],
            },
        }
    }
)

module.exports = mongoose.model('settings', settingsSchema)