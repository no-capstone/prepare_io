const express = require('express');
const cors = require('cors');
const path = require('path');

class Server {
  constructor() {

    this.app = express();
    this.port = process.env.PORT
    this.paths = {
        users: '/api/users/',
        content: '/api/content',
        companions: '/api/companions',
        questions: '/api/questions',
        answers: '/api/answers',
        settings: '/api/settings',
        scores: '/api/scores',
        stats: '/api/stats'
    };

    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.app.use(cors({ credentials: true })); 
    this.app.use(express.json());
    // this.app.use(cookieParser());
    this.app.use(express.urlencoded({ extended: true }))
  }

  // Bind controllers to routes
  routes() {    
    this.app.use(this.paths.questions, require('../routes/questions'));
    this.app.use(this.paths.users, require('../routes/users'));
    this.app.use(this.paths.settings, require('../routes/settings'));
    // this.app.use(this.paths.answers, require('../routes/answers'));
    this.app.use(this.paths.companions, require('../routes/companions'));
    // this.app.use(this.paths.content, require('../routes/content'));
    this.app.use(this.paths.scores, require('../routes/scores'));
    this.app.use(this.paths.stats, require('../routes/stats'));
    this.app.get('*', (req, res) => {
      res.sendFile(__dirname, '../client/prepare.io/index.html');
    });

  }

  listen() {

   this.app.listen(this.port, () => {
      console.log('Server running on port: ', this.port);
    });
  }
}

module.exports = Server;