const mongoose = require('mongoose')

const categorySchema = mongoose.Schema(
    {
        id: {
            type: Number,
            required: [true, ''],
        },
        category: {
            type: String,
            required: [true, ''],
        },
        
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('categories', categorySchema)