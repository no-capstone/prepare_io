const { Router } = require('express');
const {registerUser, loginUser, getCurrentUser} = require('../controllers/usersController')

const router = Router();
router.post('/register', registerUser)
router.post('/login', loginUser)
router.post('/getCurrentUser', getCurrentUser)

module.exports = router