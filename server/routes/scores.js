const { Router } = require('express');
const {getScores} = require('../controllers/scoresController')

const router = Router();
router.get('/get', getScores)
// router.get('/getQuestion', getSCQuestion)
// router.get('/getTip', getTips)

module.exports = router