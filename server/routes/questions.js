const { Router } = require('express');
const { getQuestion, calculateAnswer, getRandomQuestion, getTip } = require('../controllers/questionsController.js');

const router = Router();
router.get('/get', getQuestion);

// change to post later
router.get('/calculate', calculateAnswer);

router.get('/getRandomQuestion', getRandomQuestion);

router.post('/getTip', getTip)

module.exports = router;


