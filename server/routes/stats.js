const { Router } = require('express');
const {getGlobalStats, getUserStats} = require('../controllers/statisticsController')

const router = Router();
router.get('/globalStats', getGlobalStats)
router.get('/userStats', getUserStats)

module.exports = router