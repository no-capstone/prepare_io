const { Router } = require('express');
const { GetRandomCompanion, GetAllCompanions, GetAllCompanionsInfo } = require('../controllers/companionsController.js');

const router = Router();
router.get('/random', GetRandomCompanion);
router.get('/getAllCompanions', GetAllCompanions);
router.post('/getAllCompanionsInfo', GetAllCompanionsInfo);

module.exports = router;
