const { Router } = require('express');
const {updatePassword, updateInterviewSettings, fetchSettings} = require('../controllers/settingsController')

const router = Router();
router.put('/updatePassword', updatePassword)
router.post('/updateInterviewSettings', updateInterviewSettings)
router.post('/fetchSettings', fetchSettings)



module.exports = router