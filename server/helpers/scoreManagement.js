class ScoreManagement {

    constructor() {

        this.questionScore = 0;
        this.activeCriterias = [];

        // maximum score for all suppose to be 100
        // 4 points is hidden
        this.criterias = {
            "answer": {
                maximumScore: 21,
                penaltyScore: 0,
                granuality: 100,
                data: {},
                getScore: this.actions.answer
            },
            "usage": {
                maximumScore: 25,
                penaltyScore: 0,
                granuality: 100,
                data: {},
                getScore: this.actions.usage
            },
            "sentiment": {
                maximumScore: 25,
                penaltyScore: 0,
                granuality: 0.17,
                data: {},
                getScore: this.actions.sentiment
            },
            "emotion": {
                maximumScore: 15,
                penaltyScore: 0,
                granuality: 0.17,
                data: {},
                getScore: this.actions.emotions
            },
            "keywords": {
                maximumScore: 10,
                penaltyScore: 0,
                granuality: -0.5,
                data: {},
                relevantKeywords: [],
                getScore: this.actions.keywords
            }
            // ,
            // "entities": {
            //     maximumScore: 10,
            //     penaltyScore: 0,
            //     data: {},
            //     getScore: this.actions.entities
            // }
        }

    }

    setAvailableData(text, apiData, answerKeywords) {

        if(!apiData) {
            apiData = {
                usage: {text: ''},
                answer: ''
            }

        }

        if (text) {
            apiData['usage']['text'] = text;
        }

        if (answerKeywords) {
            let keywordsSize = answerKeywords.length;

            if(keywordsSize < 5) {
                this.criterias.answer.maximumScore = 25;
                this.criterias.sentiment.maximumScore = 30;
            } else {

            }
            apiData['answer'] = { keywords: answerKeywords, text: text};
        } else {

        }


        this.initiateCriteria(apiData);
    }


    getAverageScore() {
        return this.questionScore;
    }


    getRelatedKeywords() {
        return this.criterias['keywords'].relevantKeywords;
    }

    calculateScore() {

        let i, typesLen = this.activeCriterias.length, questionScore = 0, penaltyScore = 0;

        for (i = 0; typesLen > i; i++) {

            let currentCriteria = this.criterias[this.activeCriterias[i]];

            let score = currentCriteria.getScore();

            if (currentCriteria.penaltyScore != 0) {
                penaltyScore += currentCriteria.penaltyScore;
                // deduct some score
            }

            // delete if later
            if (score) {
                questionScore += score;
            }

        }

        console.log("Final Score ", questionScore)
        console.log("Final Penalty ", penaltyScore)
        let penaltyDeducted = (questionScore - penaltyScore);
        let finalScore = penaltyDeducted > 0 ? penaltyDeducted : 0;

        return finalScore;
    }

    initiateCriteria(combinedData) {

        const saveData = (criteria, isArray) => {
            this.activeCriterias.push(criteria);
            this.criterias[criteria].data = isArray ? combinedData[criteria] : combinedData[criteria].document ? combinedData[criteria].document : combinedData[criteria];
        }

        combinedData.usage && saveData('answer',true);
        combinedData.usage && saveData('usage');
        combinedData.sentiment && saveData('sentiment');
        combinedData.emotion && saveData('emotion');
        combinedData.keywords && saveData('keywords', true);
        // combinedData.entities && saveData('entities', true);

        this.questionScore = this.calculateScore();
    }


    getMood() {
        let sentiment = this.criterias['sentiment'].data ?? false;
        let emotions = this.criterias['emotion']?.data?.['emotion'] ?? false

        // let test = Math.max(...emotions.map(mood => mood))


        let label = 'neutral'
        let biggerScore = 0;


        if(sentiment && sentiment.label == 'positive' && sentiment.score > 0.5) {
            return 'positive';
        }
 

        if(emotions) {
            Object.keys(emotions).forEach(key => {
                if(biggerScore <= emotions[key]) {
                    biggerScore = emotions[key]
                    label = key;
                }
            });
        }

        return biggerScore > 0.2 ? label : 'neutral';

    }

    actions = (function () {

        function usage() {

            const FilterDependency = require('bad-words');
            const Filter = new FilterDependency();

            const text = this.data?.text || '';

            const containsBadWords = Filter.isProfane(text);

            if(containsBadWords) {
                this.penaltyScore = 70;
            } else {

                let regex = /\*/g;

                let status = regex.test(text);

                if(status) {
                    this.penaltyScore = 50;
                }
            }

            let endScore = this.maximumScore;
            let charNumbers = this.data?.text_characters ? this.data?.text_characters : 0;

            //  percent penalty stages of the minimum granuality
            let penaltyStages = [0.2, 0.3, 0.5, 0.8, 0.9], penaltyStagesSize = penaltyStages.length, i = 0;

            for (i = 0; i < penaltyStagesSize; i++) {
                if (charNumbers < this.granuality * penaltyStages[i]) {
                    endScore *= penaltyStages[i];
                    break;
                }
            }


            return endScore;

        }

        function answer() {
            let maximumScore = this.maximumScore;
            let keywords = this.data.keywords || [], keywordsSize = keywords.length, wordCounter = 0,i = 0;
            let text = this.data.text.toLowerCase() || '';


            for (i = 0; i < keywordsSize; i++) {

                let regex = new RegExp(`${keywords[i].toLowerCase()}`, "g");
                let status = regex.test(text);

                status && wordCounter++;
            }

            if(wordCounter == 0) {
                this.penaltyScore = maximumScore * 0.5;
            } else {
                
                let portion = keywordsSize < 5 ? 0.4 : 0.3;

                let deductedScore = maximumScore*portion;
                
                let averagePortion = deductedScore/keywordsSize;

                maximumScore = (maximumScore * portion) + (averagePortion * wordCounter)
            }

            return maximumScore;
        }

        function sentiment() {

            let score = this.data.score || 0;

            // if needed
            let label = this.data.label || 'neutral';

            let endScore = this.maximumScore;

            if (score > 0) {
                endScore *= score;
            }
            else if (score < 0) {
                endScore = endScore - (endScore * Math.abs(score));
            }
            else if (score == 0) {
                endScore *= 0.65;
            }

            return endScore;

        }

        function emotions() {

            const applyPenalty = (score) => {
                if (score > granuality) {
                    this.penaltyScore = maximumScore * score;
                }
            }

            let granuality = this.granuality;
            let maximumScore = this.maximumScore;

            let joyScore = this.data?.emotion?.joy || 0;

            let angerScore = this.data?.emotion?.anger || 0;
            let fearScore = this.data?.emotion?.fear || 0;
            let disgustScore = this.data?.emotion?.disgust || 0;


            applyPenalty(angerScore);
            applyPenalty(fearScore);
            applyPenalty(disgustScore);

            let calculatedScore = maximumScore;

            let joyFraction = maximumScore * 0.4;
            calculatedScore = (maximumScore - joyFraction) + (joyFraction * joyScore);

            return calculatedScore;

        }

        function keywords() {


            const keywordsArray = this.data, keywordslen = keywordsArray.length;

            let granuality = this.granuality;
            let maximumScore = this.maximumScore;

            let relevantKeywords = [];

            if(keywordslen < 3) {
                return maximumScore * 0.6
            }


            for (let i = 0; i < keywordslen; i++) {

                let key = keywordsArray[i];

                if(key.relevance >= 0.4) {

                    if(key.sentiment.label == 'negative' && key.sentiment.score < granuality) {
                        maximumScore  = maximumScore - (maximumScore/keywordslen);
                        continue;
                    }

                    relevantKeywords.push(key.text)
                }

            }




            this.relevantKeywords = relevantKeywords;

            return maximumScore;
        }

        function entities() {
            return 10;
        }

        return {
            emotions: emotions,
            sentiment: sentiment,
            keywords: keywords,
            entities: entities,
            usage: usage,
            answer: answer
        }
    })()
}

module.exports = ScoreManagement;