// get env params
require('dotenv').config({path:'config/.env'});
const Server = require('./models/Server');
const server = new Server();
server.listen();

const connectDB = require('./config/db');
connectDB();