const asyncHandle = require('express-async-handler')
const User = require('../models/Users')
const Settings = require('../models/Settings')

const updatePassword = asyncHandle(async (req, res) => {
    const email = JSON.parse(req.body.email)
    const pw = req.body.password
    const oldPw = req.body.oldPassword

    console.log("REQ: " + req)

    console.log("Email Adress:")
    console.log(JSON.parse(req.body.email))
    await User.findOne({
        email: email
    }).then(async (user) => {
        console.log("NEW: " + user.password)
        console.log(oldPw)
        if (user.password === oldPw) {
            user.password = pw;
            user.save()

            return res.status(200).send({ status: true, message: "Password Updated" })

        } else {
            return res.status(200).send({ status: false, message: "Old Password didn't match" })

        }
    })
})

const getSettingsCollectionCount = async () => {
    var count = await Settings.countDocuments()
    console.log("count is : " + count)
    return count
}

const updateInterviewSettings = asyncHandle(async (req, res) => {
    const companionId = req.body.companionId
    const zOpeners = req.body.zones.openersVal
    const zMiddle = req.body.zones.middleVal
    const zClosers = req.body.zones.closersVal

    Settings.findOne({ user_id: req.body.userId }).then(async (settings) => {

        if (settings) {
            console.log("user settings exists in the database")
            settings.companion_id = companionId
            settings.zone_length = {
                openers: zOpeners,
                middle: zMiddle,
                closers: zClosers
            }

            settings.save()
            console.log("new settings")
            console.log(settings)
        } else {
            var id = await getSettingsCollectionCount()
            console.log('id to be passed to settings: ' + id)

            const newSettings = new Settings({
                id: id,
                user_id: req.body.userId,
                companion_id: req.body.companionId,
                category_id: [],
                type_id: 1,
                zone_length: {
                    openers: req.body.zones.openersVal,
                    middle: req.body.zones.middleVal,
                    closers: req.body.zones.closersVal
                }
            })

            newSettings.save()

            res.status(200).send({ status: true, settings: newSettings })
        }
    })
})

const fetchSettings = asyncHandle(async (req,res) => {
    const userId = req.body.userId

    await Settings.findOne({
        user_id: userId
    }).then(async(settings)=> {
        res.status(200).send(
            {
                status: true, 
                companionId: settings.companion_id,
                openers: settings.zone_length.openers,
                middle: settings.zone_length.middle,
                closers: settings.zone_length.closers
            })
 
    })
})






module.exports = { updatePassword, updateInterviewSettings, fetchSettings }
