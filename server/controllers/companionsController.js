const Companion = require('../models/Companion');


const asyncHandle = require('express-async-handler')
// const userDbRep = require('../repository/userRepository')

// Getting random companion
const GetRandomCompanion = asyncHandle( async (req,res) => {

      // var salt = await bcrypt.genSalt(10);
      // const hashedPassword = await bcrypt.hash("22021510", salt)
      // console.log(hashedPassword)

  try {

    // const { email, password } = req.body;

    // if (!email || !password) {
      return res.status(200).send({ status: true, msg: "All good", companion: {} })
    


  } catch (err) {
    console.log(err)
    return res.status(500).send({ status: false, msg: err });
  }

});

const GetAllCompanions = asyncHandle( async (req,res) => {

  await Companion.find({}).lean().then((result)=>{
    
    console.log(result)

    return res.status(200).send({ status: true, msg: "All good", companions: result})  
  })
  
});

const GetAllCompanionsInfo = asyncHandle(async(req,res)=>{

  const companionId = req.body.companionId
  console.log("companion id: " + req.body.companionId)

  await Companion.findOne({
    id: companionId
  }).then(async(companion)=>{
    res.status(200).send(
      {
        status: true,
        compName: companion.name,
        compTitle: companion.title,
        compJobPlace: companion.info.job_place,
        compImg: companion.info.profileImg,
        agenda: companion.agenda

      }
    )
  })

})



module.exports = {
    GetRandomCompanion, GetAllCompanions, GetAllCompanionsInfo
 }