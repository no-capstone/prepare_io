const asyncHandle = require('express-async-handler')
const { count } = require('../models/Users')
const User = require('../models/Users')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs');


// We will use this function to
const getUserCollectionCount = async() => {
    var count = await User.countDocuments()
    console.log("count is : " + count)
    return count
}

const hashUserPass = async(pass) => {
    const saltRounds = 10;
    const hashedPass = await bcrypt.hash(pass, saltRounds)

    return hashedPass
}

const registerUser = asyncHandle(async (req, res) => {
    // does this need to be awaited here??
    User.findOne({ email: req.body.email }).then(async (user) => {
        if (user) {
            // Return error if email exists (change to 400 error?)
            console.log("Error adding User: email exists")
            return res.status(400).send({status: false, msg: "Error: Email Already Exists"})
        } else {
            // We count the number of documents inside the collection to determine the id to be passed
            var id = await getUserCollectionCount()
            // console.log('id to be passed to create user: ' + id)
    
            const password = req.body.password
            const hashedPass = await hashUserPass(password)

            // Create the User
            const newUser = new User({
                id: id,
                email: req.body.email,
                firstName: req.body.fName,
                lastName: req.body.lName,
                password: hashedPass
            });

            // console.log(newUser.password)
            console.log("Added User")
            newUser.save();
            // Do we return status 200 here?
            return res.status(200).send({status: true, data: newUser})
        }
    })
})

const getCurrentUser = asyncHandle(async (req, res) => {
    const token = req.body.token
    console.log("token is")
    console.log(token)

    const user = jwt.decode(JSON.parse(token))
    // const user = jwt.decode(token, )
    console.log("user is")
    console.log(user)
    return res.status(200).send({ status: true, currentUser: user})

})

const loginUser = asyncHandle(async (req, res) => {
    
    try {
        const user = await User.findOne({ 
            email: req.body.email
        })

        // User password is hashed in db
        const password = req.body.password

        if(await bcrypt.compare(password, user.password)) {
            console.log("user exists in db")
            const token = jwt.sign({
                email: user.email,
                id: user.id
            }, 'makeMoreSecure')
                
            return res.status(200).send({status: true, token: token, email: user.email}) 
        }

        console.log("user does not exist in db")
        return res.status(200).send({ status: false, errorMsg: "Check your credentials and try again!" })
    } catch (error) {
        console.log("ERROR: " + error)
        return res.status(500).send({ status: false, errorMsg: "error: " + error })
    }
})

module.exports = {registerUser, loginUser, getCurrentUser}