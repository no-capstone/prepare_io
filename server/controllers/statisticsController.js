const asyncHandle = require('express-async-handler')
const user = require('../models/Users')
const session = require('../models/Session')
const score = require('../models/Score')

const Companion = require('../models/Companion');
const { response } = require('express');
const { where } = require('../models/Question');


const getGlobalStats = asyncHandle(async (req, res) => {
    console.log("Global Stats...");

    let totalInterview = 0;
    let usersAmt = 0;


    var counter = session.find();
    counter.count(function (error, count){
        if (error) console.log("Error occured: " + error)
        else {
            totalInterview = count;
            console.log("Total interviews! " + totalInterview);
            
        }
        var userCounter = user.find();
    userCounter.count(function (error, count){
        if (error) console.log("Error occured: " + error)
        else {
            usersAmt = count;
            console.log("User count! " + usersAmt);
        }
        res.status(200).send(
            {
                status: true, 
                userCount: usersAmt,
                total: totalInterview
            })
    })
    })
    
    // console.log("Stats: " + usersAmt + " " + totalInterview);
    //          res.status(200).send(
    //             {
    //                 status: true, 
    //                 userCount: usersAmt,
    //                 total: totalInterview
    //             })

})

const getUserStats = asyncHandle(async (req, res) =>{
    console.log("Let's Begin...");
    const userId = req.query.userId;
    let scoreAverage = 0;
    let highScore = 0;
    let sessionsHolder = [];
    let scores = [];
    let tempAdd = 0;
    let size = 0;
    console.log("The userId: " + userId);

    await session.find({user_id: userId}).then((result)=>{

        for (let i = 0; i < result.length; i++) {
            sessionsHolder.push(result[i].id); 
        }

        const temp = score.find({session_id: sessionsHolder}).then((result2)=>{
            for (let t = 0; t < result2.length; t++) {
                scores.push(result2[t].score_p);
                
            }

            highScore = Math.max.apply(null, scores)
            highScore = Math.round(highScore)
            size = scores.length
            console.log("High and size: " + highScore + " " + size);

            for (let k = 0; k < scores.length; k++) {
                tempAdd += scores[k];
            }
            scoreAverage = tempAdd / size
            scoreAverage = Math.round(scoreAverage)
            console.log("Average & Highs: " + highScore + " " + scoreAverage);
            return res.status(200).send(
                {
                    status: true, 
                    average: scoreAverage,
                    high: highScore
                })

        })
    })
})
module.exports = { getGlobalStats, getUserStats}
