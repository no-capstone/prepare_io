const asyncHandle = require('express-async-handler')
const Question = require('../models/Question')
const Answers = require('../models/Answers')
const Score = require('../models/Score')
const Session = require('../models/Session')
const Tip = require('../models/Tips')
const { getAnalysisByText, calculateScore } = require('../services/textAnalyzer')
const { constructFilepath } = require('ibm-cloud-sdk-core')
// const {  } = require('../helpers/scoreManagement')

// Getting score
const getQuestion = asyncHandle(async (req, res) => {

  try {

    if (!req.query) {
      return res.status(500).send({ status: false, msg: "Not enough info" })
    }

    let zone = req.query.zone ? req.query.zone : 'openers';
    let companionId = req.query.companionId ? req.query.companionId : 1;
    let used_questions = req.query.used_questions ? req.query.used_questions.split(',') : [];

    let session = req.query.session_start ? req.query.session_start : false;
  

    const questionObjects = await Question.find({ $and: [ { zone: zone }, { id: { $nin: used_questions } },{comp_id: { $in: companionId }} ] }) || {};

    var max = questionObjects.length
    var questionIdToFind

    if (session) {
      questionIdToFind = 0
    } else {
      questionIdToFind = 1 + Math.floor(Math.random() * (max - 1 + 1))
    }
  
    console.log(questionIdToFind)

    const questionObject = questionObjects[questionIdToFind]


    if (!questionObject) {
      return res.status(500).send({ status: false, msg: "Not enough info", askedQuestion: question_id  })
    }


    const { id, text } = questionObject;


    console.log(questionObject);

    if (text) {

      let data = { id: id, message: text };

      if (session) {
        let count = await getCollectionCount(Session) + 1

        let sessionObj = new Session({id: count,user_id: 1 });
        sessionObj.save(function (err, session) {
          if (err) return console.error(err);
          console.log(session.user_id + " user_id saved to session collection.");
        });


        // get session id
        data['session_id'] = sessionObj.id;
      }

      return res.status(200).send({ status: true, data: data })
    }

    return res.status(500).send({ status: false, msg: "Something went wrong", askedQuestion: question_id })

  } catch (err) {
    console.log(err)
    return res.status(500).send({ status: false, msg: err, askedQuestion: question_id  });
  }

});
const getCollectionCount = async (collection) => {
  var count = await collection.countDocuments()
  return count
}


const getRandomQuestion = asyncHandle(async(req, res) => {
    
  let num = Math.floor(Math.random() * 174) + 1

  await Question.findOne({id: num}).lean().then((result)=>{
    return res.status(200).send({ status: true, msg: "All good", questions: result})  
  })
});


const getTip= asyncHandle(async (req,res) => {
  const tipId = req.body.tipId

  await Tip.findOne({
      id: tipId
  }).then(async(tip)=> {
      res.status(200).send(
          {
              status: true, 
              tips: tip.tips
          })

  })
})



// http://localhost:8080/api/questions/calculate?question_id=1&zone=openers&user_answer=My%20name%20is%20Aleksei,%20it%20is%20nice%20to%20meet%20you
const calculateAnswer = asyncHandle(async (req, res) => {

  try {

    if (!req.query) {
      return res.status(500).send({ status: false, msg: "Not enough info" })
    }

    let zone = req.query.zone ? req.query.zone : "openers";

    let user_answer = req.query.user_answer ? req.query.user_answer : false;
    let question_id = req.query.question_id ? req.query.question_id : false;
    let companionId = req.query.companionId ? req.query.companionId : 1;
    let used_questions = req.query.used_questions ? req.query.used_questions.split(',') : [];
    const session_id = req.query.session_id ? req.query.session_id : false;

    if (!user_answer || !question_id) {
      return res.status(500).send({ status: false, msg: "Not enough info" })
    }


    const questionObject = await Question.findOne({ id: question_id }) || {};


    const answerObject = await Answers.findOne({ id: questionObject?.answer_id });


    if(user_answer.length < 5) {
      return res.status(500).send({ status: false, msg: "Could you elaborate a little more?", askedQuestion: question_id  })
    }

    getAnalysisByText(user_answer, answerObject.keywords, async function (response) {

      if (!response) {
        return res.status(500).send({ status: false, msg: "Not enough info", askedQuestion: question_id  })
      }


      let { keywords, score, mood } = response;

      let status = await saveQuestionScore(question_id, session_id, score, user_answer)


      let followUpQuestion = {}
      let computerResponse = score > 50 ? answerObject.good : answerObject.bad;

      if (keywords.length != 0) {

        let regexString = keywords.join("|");

        followUpResponse = await Question.findOne({ zone: zone,comp_id: companionId,
          "keywords": {
            "$regex": regexString,
            "$options": "i"
          }
        }).where('id').nin(used_questions);

        if (followUpResponse) {
          followUpQuestion = { id: followUpResponse.id, text: followUpResponse.text }
        }

      }

      let data = {
        id: parseInt(question_id),
        score: score,
        mood: mood,
        computerResponse: computerResponse,
        followUpQuestion: followUpQuestion
      }


      res.status(200).send({ status: true, data: data })
    });




    const saveQuestionScore = async (question_id,session_id,score,user_answer) => {

      if(!question_id || !session_id || !score || !user_answer ) {
        return false;
      }


      let count = await getCollectionCount(Score) + 1

      let scoreObj = new Score({id: count,quest_id: question_id, session_id: session_id,score_p: score, user_answer: user_answer });
      scoreObj.save(function (err, score) {
        if (err) return console.error(err);

      });



      return scoreObj;
      // get score id



    }


  } catch (err) {
    console.log(err)
    return res.status(500).send({ status: false, msg: err, askedQuestion: question_id  });
  }

});




module.exports = {
  getQuestion,
  calculateAnswer,
  getRandomQuestion,
  getTip
}