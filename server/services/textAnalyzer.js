

const NaturalLanguageUnderstandingV1 = require('ibm-watson/natural-language-understanding/v1');
const { IamAuthenticator } = require('ibm-watson/auth');

const ScoreManagement = require('../helpers/scoreManagement');

// delete this part later 
// require('dotenv').config({path:'../config/.env'});

const naturalLanguageUnderstanding = new NaturalLanguageUnderstandingV1({
  version: '2022-04-07',
  language: 'en',
  authenticator: new IamAuthenticator({
    apikey: process.env.WATSON_API_KEY,
  }),
  serviceUrl: process.env.WATSON_URL
});

const analyzeParams = {
  'text': '',
  'features': {
    'sentiment': {},
    // 'categories': {
    //   'limit': 3
    // },
    'emotion': {},
    'entities': {},
    'keywords': {
      'sentiment': true,
      // 'emotion': true,
      'limit': 5
    }
    // 'keywords': {
    //   'sentiment': true,
    //   'emotion': true,
    //   'limit': 5
    // }
  }
};

function calculateScore(text, result, answer = null) {
  const scoreManagement = new ScoreManagement();
  scoreManagement.setAvailableData(text, result, answer)
  let averageScore = scoreManagement.getAverageScore();

  let relatedKeywords = scoreManagement.getRelatedKeywords();
  let mood = scoreManagement.getMood();
  return {score: averageScore, keywords: relatedKeywords, mood: mood};
}

function getExpectedResponse(text) {

  if (text == '') {
    return 'Is it everything you wanted to say?'
  }

  return false;

}



function getAnalysisByText(text, answer, callback) {

  analyzeParams.text = text;

  let result = {};

  naturalLanguageUnderstanding.analyze(analyzeParams)
    .then(function (analysisResults) {
      result = analysisResults.result;
      let data = calculateScore(text, result, answer);
      return callback(data);
    })
    .catch(function (err) {

      console.log("data");
      console.log(err)
      let data = calculateScore(text, false, answer);
      console.log(data);
      return callback(data);
    });

}



module.exports = {
  getAnalysisByText
}



